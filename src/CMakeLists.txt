set_source_files_properties(TreeView.qml PROPERTIES
    QT_QML_SOURCE_VERSIONS "2.15;6.0"
)

qt_internal_add_qml_module(QuickTreeView
    URI "QtQuick.TreeView"
    VERSION "${CMAKE_PROJECT_VERSION}"
    CLASS_NAME QQuickTreeViewPlugin
    PLUGIN_TARGET qquicktreeviewplugin
    QML_FILES
        TreeView.qml
    SOURCES
        qquicktreemodeladaptor.cpp qquicktreemodeladaptor_p.h
        qquicktreeview.cpp qquicktreeview_p.h
        qquicktreeview_p_p.h
    LIBRARIES
        Qt::Core
        Qt::Gui
        Qt::Qml
        Qt::Quick
        Qt::QuickPrivate
)

qt_internal_add_docs(qquicktreeviewplugin
     doc/treeview.qdocconf
 )

